.. SPDX-License-Identifier: CC-BY-4.0

Guest Scheduler Operations
==========================

.. doxygengroup:: guest_scheduler_operations
   :project: Xen
