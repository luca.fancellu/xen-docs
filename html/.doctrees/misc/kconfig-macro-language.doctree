��dp      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�h �section���)��}�(hhh]�(h �title���)��}�(h�Kconfig macro language�h]�h �Text����Kconfig macro language�����}�(hh�parent�hhh�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�?/home/lucfan01/prj_xen/xen/docs/misc/kconfig-macro-language.rst�hKubh
)��}�(hhh]�(h)��}�(h�Concept�h]�h�Concept�����}�(hh0hh.hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh+hhhh*hKubh �	paragraph���)��}�(h��The basic idea was inspired by Make. When we look at Make, we notice sort of
two languages in one. One language describes dependency graphs consisting of
targets and prerequisites. The other is a macro language for performing textual
substitution.�h]�h��The basic idea was inspired by Make. When we look at Make, we notice sort of
two languages in one. One language describes dependency graphs consisting of
targets and prerequisites. The other is a macro language for performing textual
substitution.�����}�(hh@hh>hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKhh+hhubh=)��}�(h�pThere is clear distinction between the two language stages. For example, you
can write a makefile like follows::�h]�h�oThere is clear distinction between the two language stages. For example, you
can write a makefile like follows:�����}�(h�oThere is clear distinction between the two language stages. For example, you
can write a makefile like follows:�hhLhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKhh+hhubh �literal_block���)��}�(h�PAPP := foo
SRC := foo.c
CC := gcc

$(APP): $(SRC)
        $(CC) -o $(APP) $(SRC)�h]�h�PAPP := foo
SRC := foo.c
CC := gcc

$(APP): $(SRC)
        $(CC) -o $(APP) $(SRC)�����}�(hhhh]ubah}�(h]�h!]�h#]�h%]�h']��	xml:space��preserve�uh)h[hKhh+hhhh*ubh=)��}�(h��The macro language replaces the variable references with their expanded form,
and handles as if the source file were input like follows::�h]�h��The macro language replaces the variable references with their expanded form,
and handles as if the source file were input like follows:�����}�(h��The macro language replaces the variable references with their expanded form,
and handles as if the source file were input like follows:�hhmhhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKhh+hhubh\)��}�(h�#foo: foo.c
        gcc -o foo foo.c�h]�h�#foo: foo.c
        gcc -o foo foo.c�����}�(hhhh|ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hKhh+hhhh*ubh=)��}�(h�RThen, Make analyzes the dependency graph and determines the targets to be
updated.�h]�h�RThen, Make analyzes the dependency graph and determines the targets to be
updated.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKhh+hhubh=)��}�(h�\The idea is quite similar in Kconfig - it is possible to describe a Kconfig
file like this::�h]�h�[The idea is quite similar in Kconfig - it is possible to describe a Kconfig
file like this:�����}�(h�[The idea is quite similar in Kconfig - it is possible to describe a Kconfig
file like this:�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK hh+hhubh\)��}�(h�aCC := gcc

config CC_HAS_FOO
        def_bool $(shell, $(srctree)/scripts/gcc-check-foo.sh $(CC))�h]�h�aCC := gcc

config CC_HAS_FOO
        def_bool $(shell, $(srctree)/scripts/gcc-check-foo.sh $(CC))�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hK#hh+hhhh*ubh=)��}�(h�YThe macro language in Kconfig processes the source file into the following
intermediate::�h]�h�XThe macro language in Kconfig processes the source file into the following
intermediate:�����}�(h�XThe macro language in Kconfig processes the source file into the following
intermediate:�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK(hh+hhubh\)��}�(h�$config CC_HAS_FOO
        def_bool y�h]�h�$config CC_HAS_FOO
        def_bool y�����}�(hhhh�ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hK+hh+hhhh*ubh=)��}�(h�vThen, Kconfig moves onto the evaluation stage to resolve inter-symbol
dependency as explained in kconfig-language.txt.�h]�h�vThen, Kconfig moves onto the evaluation stage to resolve inter-symbol
dependency as explained in kconfig-language.txt.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK.hh+hhubeh}�(h]��concept�ah!]�h#]��concept�ah%]�h']�uh)h	hhhhhh*hKubh
)��}�(hhh]�(h)��}�(h�	Variables�h]�h�	Variables�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhh�hhhh*hK3ubh=)��}�(hXx  Like in Make, a variable in Kconfig works as a macro variable.  A macro
variable is expanded "in place" to yield a text string that may then be
expanded further. To get the value of a variable, enclose the variable name in
$( ). The parentheses are required even for single-letter variable names; $X is
a syntax error. The curly brace form as in ${CC} is not supported either.�h]�hX|  Like in Make, a variable in Kconfig works as a macro variable.  A macro
variable is expanded “in place” to yield a text string that may then be
expanded further. To get the value of a variable, enclose the variable name in
$( ). The parentheses are required even for single-letter variable names; $X is
a syntax error. The curly brace form as in ${CC} is not supported either.�����}�(hh�hh�hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK5hh�hhubh=)��}�(h�_There are two types of variables: simply expanded variables and recursively
expanded variables.�h]�h�_There are two types of variables: simply expanded variables and recursively
expanded variables.�����}�(hj	  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK;hh�hhubh=)��}�(h��A simply expanded variable is defined using the := assignment operator. Its
righthand side is expanded immediately upon reading the line from the Kconfig
file.�h]�h��A simply expanded variable is defined using the := assignment operator. Its
righthand side is expanded immediately upon reading the line from the Kconfig
file.�����}�(hj  hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK>hh�hhubh=)��}�(h��A recursively expanded variable is defined using the = assignment operator.
Its righthand side is simply stored as the value of the variable without
expanding it in any way. Instead, the expansion is performed when the variable
is used.�h]�h��A recursively expanded variable is defined using the = assignment operator.
Its righthand side is simply stored as the value of the variable without
expanding it in any way. Instead, the expansion is performed when the variable
is used.�����}�(hj%  hj#  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKBhh�hhubh=)��}�(h��There is another type of assignment operator; += is used to append text to a
variable. The righthand side of += is expanded immediately if the lefthand
side was originally defined as a simple variable. Otherwise, its evaluation is
deferred.�h]�h��There is another type of assignment operator; += is used to append text to a
variable. The righthand side of += is expanded immediately if the lefthand
side was originally defined as a simple variable. Otherwise, its evaluation is
deferred.�����}�(hj3  hj1  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKGhh�hhubh=)��}�(h�CThe variable reference can take parameters, in the following form::�h]�h�BThe variable reference can take parameters, in the following form:�����}�(h�BThe variable reference can take parameters, in the following form:�hj?  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKLhh�hhubh\)��}�(h�$(name,arg1,arg2,arg3)�h]�h�$(name,arg1,arg2,arg3)�����}�(hhhjN  ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hKNhh�hhhh*ubh=)��}�(h��You can consider the parameterized reference as a function. (more precisely,
"user-defined function" in contrast to "built-in function" listed below).�h]�h��You can consider the parameterized reference as a function. (more precisely,
“user-defined function” in contrast to “built-in function” listed below).�����}�(hj^  hj\  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKPhh�hhubh=)��}�(hX"  Useful functions must be expanded when they are used since the same function is
expanded differently if different parameters are passed. Hence, a user-defined
function is defined using the = assignment operator. The parameters are
referenced within the body definition with $(1), $(2), etc.�h]�hX"  Useful functions must be expanded when they are used since the same function is
expanded differently if different parameters are passed. Hence, a user-defined
function is defined using the = assignment operator. The parameters are
referenced within the body definition with $(1), $(2), etc.�����}�(hjl  hjj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKShh�hhubh=)��}�(h��In fact, recursively expanded variables and user-defined functions are the same
internally. (In other words, "variable" is "function with zero argument".)
When we say "variable" in a broad sense, it includes "user-defined function".�h]�h��In fact, recursively expanded variables and user-defined functions are the same
internally. (In other words, “variable” is “function with zero argument”.)
When we say “variable” in a broad sense, it includes “user-defined function”.�����}�(hjz  hjx  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKXhh�hhubeh}�(h]��	variables�ah!]�h#]��	variables�ah%]�h']�uh)h	hhhhhh*hK3ubh
)��}�(hhh]�(h)��}�(h�Built-in functions�h]�h�Built-in functions�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj�  hhhh*hK^ubh=)��}�(h�nLike Make, Kconfig provides several built-in functions. Every function takes a
particular number of arguments.�h]�h�nLike Make, Kconfig provides several built-in functions. Every function takes a
particular number of arguments.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK`hj�  hhubh=)��}�(hXG  In Make, every built-in function takes at least one argument. Kconfig allows
zero argument for built-in functions, such as $(fileno), $(lineno). You could
consider those as "built-in variable", but it is just a matter of how we call
it after all. Let's say "built-in function" here to refer to natively supported
functionality.�h]�hXQ  In Make, every built-in function takes at least one argument. Kconfig allows
zero argument for built-in functions, such as $(fileno), $(lineno). You could
consider those as “built-in variable”, but it is just a matter of how we call
it after all. Let’s say “built-in function” here to refer to natively supported
functionality.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKchj�  hhubh=)��}�(h�<Kconfig currently supports the following built-in functions.�h]�h�<Kconfig currently supports the following built-in functions.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKihj�  hhubh �block_quote���)��}�(hhh]�(h �bullet_list���)��}�(hhh]�h �	list_item���)��}�(h�$(shell,command)
�h]�h=)��}�(h�$(shell,command)�h]�h�$(shell,command)�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKkhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubah}�(h]�h!]�h#]�h%]�h']��bullet��-�uh)j�  hh*hKkhj�  ubj�  )��}�(hhh]�h=)��}�(hX_  The "shell" function accepts a single argument that is expanded and passed
to a subshell for execution. The standard output of the command is then read
and returned as the value of the function. Every newline in the output is
replaced with a space. Any trailing newlines are deleted. The standard error
is not returned, nor is any program exit status.�h]�hXc  The “shell” function accepts a single argument that is expanded and passed
to a subshell for execution. The standard output of the command is then read
and returned as the value of the function. Every newline in the output is
replaced with a space. Any trailing newlines are deleted. The standard error
is not returned, nor is any program exit status.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKmhj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubj�  )��}�(hhh]�j�  )��}�(h�$(info,text)
�h]�h=)��}�(h�$(info,text)�h]�h�$(info,text)�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKshj  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj  ubah}�(h]�h!]�h#]�h%]�h']�j�  j�  uh)j�  hh*hKshj�  ubj�  )��}�(hhh]�h=)��}�(h�eThe "info" function takes a single argument and prints it to stdout.
It evaluates to an empty string.�h]�h�iThe “info” function takes a single argument and prints it to stdout.
It evaluates to an empty string.�����}�(hj2  hj0  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKuhj-  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubj�  )��}�(hhh]�j�  )��}�(h�$(warning-if,condition,text)
�h]�h=)��}�(h�$(warning-if,condition,text)�h]�h�$(warning-if,condition,text)�����}�(hjM  hjK  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKxhjG  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hjD  ubah}�(h]�h!]�h#]�h%]�h']�j�  j�  uh)j�  hh*hKxhj�  ubj�  )��}�(hhh]�h=)��}�(h��The "warning-if" function takes two arguments. If the condition part is "y",
the text part is sent to stderr. The text is prefixed with the name of the
current Kconfig file and the current line number.�h]�h��The “warning-if” function takes two arguments. If the condition part is “y”,
the text part is sent to stderr. The text is prefixed with the name of the
current Kconfig file and the current line number.�����}�(hjj  hjh  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hKzhje  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubj�  )��}�(hhh]�j�  )��}�(h�$(error-if,condition,text)
�h]�h=)��}�(h�$(error-if,condition,text)�h]�h�$(error-if,condition,text)�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK~hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj|  ubah}�(h]�h!]�h#]�h%]�h']�j�  j�  uh)j�  hh*hK~hj�  ubj�  )��}�(hhh]�h=)��}�(h�{The "error-if" function is similar to "warning-if", but it terminates the
parsing immediately if the condition part is "y".�h]�h��The “error-if” function is similar to “warning-if”, but it terminates the
parsing immediately if the condition part is “y”.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubj�  )��}�(hhh]�j�  )��}�(h�$(filename)
�h]�h=)��}�(h�$(filename)�h]�h�$(filename)�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�j�  j�  uh)j�  hh*hK�hj�  ubj�  )��}�(hhh]�h=)��}�(h�\The 'filename' takes no argument, and $(filename) is expanded to the file
name being parsed.�h]�h�`The ‘filename’ takes no argument, and $(filename) is expanded to the file
name being parsed.�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubj�  )��}�(hhh]�j�  )��}�(h�
$(lineno)
�h]�h=)��}�(h�	$(lineno)�h]�h�	$(lineno)�����}�(hj�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj�  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubah}�(h]�h!]�h#]�h%]�h']�j�  j�  uh)j�  hh*hK�hj�  ubj�  )��}�(hhh]�h=)��}�(h�ZThe 'lineno' takes no argument, and $(lineno) is expanded to the line number
being parsed.�h]�h�^The ‘lineno’ takes no argument, and $(lineno) is expanded to the line number
being parsed.�����}�(hj  hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj  ubah}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  ubeh}�(h]�h!]�h#]�h%]�h']�uh)j�  hj�  hhhh*hNubeh}�(h]��built-in-functions�ah!]�h#]��built-in functions�ah%]�h']�uh)h	hhhhhh*hK^ubh
)��}�(hhh]�(h)��}�(h�Make vs Kconfig�h]�h�Make vs Kconfig�����}�(hj7  hj5  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj2  hhhh*hK�ubh=)��}�(h�\Kconfig adopts Make-like macro language, but the function call syntax is
slightly different.�h]�h�\Kconfig adopts Make-like macro language, but the function call syntax is
slightly different.�����}�(hjE  hjC  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj2  hhubh=)��}�(h�)A function call in Make looks like this::�h]�h�(A function call in Make looks like this:�����}�(h�(A function call in Make looks like this:�hjQ  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj2  hhubh\)��}�(h�$(func-name arg1,arg2,arg3)�h]�h�$(func-name arg1,arg2,arg3)�����}�(hhhj`  ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hK�hj2  hhhh*ubh=)��}�(hXm  The function name and the first argument are separated by at least one
whitespace. Then, leading whitespaces are trimmed from the first argument,
while whitespaces in the other arguments are kept. You need to use a kind of
trick to start the first parameter with spaces. For example, if you want
to make "info" function print "  hello", you can write like follows::�h]�hXt  The function name and the first argument are separated by at least one
whitespace. Then, leading whitespaces are trimmed from the first argument,
while whitespaces in the other arguments are kept. You need to use a kind of
trick to start the first parameter with spaces. For example, if you want
to make “info” function print ”  hello”, you can write like follows:�����}�(hXl  The function name and the first argument are separated by at least one
whitespace. Then, leading whitespaces are trimmed from the first argument,
while whitespaces in the other arguments are kept. You need to use a kind of
trick to start the first parameter with spaces. For example, if you want
to make "info" function print "  hello", you can write like follows:�hjn  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj2  hhubh\)��}�(h�Aempty :=
space := $(empty) $(empty)
$(info $(space)$(space)hello)�h]�h�Aempty :=
space := $(empty) $(empty)
$(info $(space)$(space)hello)�����}�(hhhj}  ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hK�hj2  hhhh*ubh=)��}�(h��Kconfig uses only commas for delimiters, and keeps all whitespaces in the
function call. Some people prefer putting a space after each comma delimiter::�h]�h��Kconfig uses only commas for delimiters, and keeps all whitespaces in the
function call. Some people prefer putting a space after each comma delimiter:�����}�(h��Kconfig uses only commas for delimiters, and keeps all whitespaces in the
function call. Some people prefer putting a space after each comma delimiter:�hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj2  hhubh\)��}�(h�$(func-name, arg1, arg2, arg3)�h]�h�$(func-name, arg1, arg2, arg3)�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hK�hj2  hhhh*ubh=)��}�(h��In this case, "func-name" will receive " arg1", " arg2", " arg3". The presence
of leading spaces may matter depending on the function. The same applies to
Make - for example, $(subst .c, .o, $(sources)) is a typical mistake; it
replaces ".c" with " .o".�h]�hX  In this case, “func-name” will receive ” arg1”, ” arg2”, ” arg3”. The presence
of leading spaces may matter depending on the function. The same applies to
Make - for example, $(subst .c, .o, $(sources)) is a typical mistake; it
replaces “.c” with ” .o”.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj2  hhubh=)��}�(h�`In Make, a user-defined function is referenced by using a built-in function,
'call', like this::�h]�h�cIn Make, a user-defined function is referenced by using a built-in function,
‘call’, like this:�����}�(h�_In Make, a user-defined function is referenced by using a built-in function,
'call', like this:�hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj2  hhubh\)��}�(h�$(call my-func,arg1,arg2,arg3)�h]�h�$(call my-func,arg1,arg2,arg3)�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hK�hj2  hhhh*ubh=)��}�(h�Kconfig invokes user-defined functions and built-in functions in the same way.
The omission of 'call' makes the syntax shorter.�h]�h��Kconfig invokes user-defined functions and built-in functions in the same way.
The omission of ‘call’ makes the syntax shorter.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj2  hhubh=)��}�(hX	  In Make, some functions treat commas verbatim instead of argument separators.
For example, $(shell echo hello, world) runs the command "echo hello, world".
Likewise, $(info hello, world) prints "hello, world" to stdout. You could say
this is _useful_ inconsistency.�h]�hX  In Make, some functions treat commas verbatim instead of argument separators.
For example, $(shell echo hello, world) runs the command “echo hello, world”.
Likewise, $(info hello, world) prints “hello, world” to stdout. You could say
this is _useful_ inconsistency.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj2  hhubh=)��}�(h��In Kconfig, for simpler implementation and grammatical consistency, commas that
appear in the $( ) context are always delimiters. It means::�h]�h��In Kconfig, for simpler implementation and grammatical consistency, commas that
appear in the $( ) context are always delimiters. It means:�����}�(h��In Kconfig, for simpler implementation and grammatical consistency, commas that
appear in the $( ) context are always delimiters. It means:�hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj2  hhubh\)��}�(h�$(shell, echo hello, world)�h]�h�$(shell, echo hello, world)�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hK�hj2  hhhh*ubh=)��}�(h��is an error because it is passing two parameters where the 'shell' function
accepts only one. To pass commas in arguments, you can use the following trick::�h]�h��is an error because it is passing two parameters where the ‘shell’ function
accepts only one. To pass commas in arguments, you can use the following trick:�����}�(h��is an error because it is passing two parameters where the 'shell' function
accepts only one. To pass commas in arguments, you can use the following trick:�hj  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj2  hhubh\)��}�(h�-comma := ,
$(shell, echo hello$(comma) world)�h]�h�-comma := ,
$(shell, echo hello$(comma) world)�����}�(hhhj  ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hK�hj2  hhhh*ubeh}�(h]��make-vs-kconfig�ah!]�h#]��make vs kconfig�ah%]�h']�uh)h	hhhhhh*hK�ubh
)��}�(hhh]�(h)��}�(h�Caveats�h]�h�Caveats�����}�(hj6  hj4  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)hhj1  hhhh*hK�ubh=)��}�(h��A variable (or function) cannot be expanded across tokens. So, you cannot use
a variable as a shorthand for an expression that consists of multiple tokens.
The following works::�h]�h��A variable (or function) cannot be expanded across tokens. So, you cannot use
a variable as a shorthand for an expression that consists of multiple tokens.
The following works:�����}�(h��A variable (or function) cannot be expanded across tokens. So, you cannot use
a variable as a shorthand for an expression that consists of multiple tokens.
The following works:�hjB  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj1  hhubh\)��}�(h�cRANGE_MIN := 1
RANGE_MAX := 3

config FOO
        int "foo"
        range $(RANGE_MIN) $(RANGE_MAX)�h]�h�cRANGE_MIN := 1
RANGE_MAX := 3

config FOO
        int "foo"
        range $(RANGE_MIN) $(RANGE_MAX)�����}�(hhhjQ  ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hK�hj1  hhhh*ubh=)��}�(h�"But, the following does not work::�h]�h�!But, the following does not work:�����}�(h�!But, the following does not work:�hj_  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj1  hhubh\)��}�(h�CRANGES := 1 3

config FOO
        int "foo"
        range $(RANGES)�h]�h�CRANGES := 1 3

config FOO
        int "foo"
        range $(RANGES)�����}�(hhhjn  ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hK�hj1  hhhh*ubh=)��}�(h�WA variable cannot be expanded to any keyword in Kconfig.  The following does
not work::�h]�h�VA variable cannot be expanded to any keyword in Kconfig.  The following does
not work:�����}�(h�VA variable cannot be expanded to any keyword in Kconfig.  The following does
not work:�hj|  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj1  hhubh\)��}�(h�JMY_TYPE := tristate

config FOO
        $(MY_TYPE) "foo"
        default y�h]�h�JMY_TYPE := tristate

config FOO
        $(MY_TYPE) "foo"
        default y�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hK�hj1  hhhh*ubh=)��}�(h��Obviously from the design, $(shell command) is expanded in the textual
substitution phase. You cannot pass symbols to the 'shell' function.�h]�h��Obviously from the design, $(shell command) is expanded in the textual
substitution phase. You cannot pass symbols to the ‘shell’ function.�����}�(hj�  hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj1  hhubh=)��}�(h�)The following does not work as expected::�h]�h�(The following does not work as expected:�����}�(h�(The following does not work as expected:�hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj1  hhubh\)��}�(h��config ENDIAN_FLAG
        string
        default "-mbig-endian" if CPU_BIG_ENDIAN
        default "-mlittle-endian" if CPU_LITTLE_ENDIAN

config CC_HAS_ENDIAN_FLAG
        def_bool $(shell $(srctree)/scripts/gcc-check-flag ENDIAN_FLAG)�h]�h��config ENDIAN_FLAG
        string
        default "-mbig-endian" if CPU_BIG_ENDIAN
        default "-mlittle-endian" if CPU_LITTLE_ENDIAN

config CC_HAS_ENDIAN_FLAG
        def_bool $(shell $(srctree)/scripts/gcc-check-flag ENDIAN_FLAG)�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hK�hj1  hhhh*ubh=)��}�(h�SInstead, you can do like follows so that any function call is statically
expanded::�h]�h�RInstead, you can do like follows so that any function call is statically
expanded:�����}�(h�RInstead, you can do like follows so that any function call is statically
expanded:�hj�  hhhNhNubah}�(h]�h!]�h#]�h%]�h']�uh)h<hh*hK�hj1  hhubh\)��}�(h��config CC_HAS_ENDIAN_FLAG
        bool
        default $(shell $(srctree)/scripts/gcc-check-flag -mbig-endian) if CPU_BIG_ENDIAN
        default $(shell $(srctree)/scripts/gcc-check-flag -mlittle-endian) if CPU_LITTLE_ENDIAN�h]�h��config CC_HAS_ENDIAN_FLAG
        bool
        default $(shell $(srctree)/scripts/gcc-check-flag -mbig-endian) if CPU_BIG_ENDIAN
        default $(shell $(srctree)/scripts/gcc-check-flag -mlittle-endian) if CPU_LITTLE_ENDIAN�����}�(hhhj�  ubah}�(h]�h!]�h#]�h%]�h']�hkhluh)h[hK�hj1  hhhh*ubeh}�(h]��caveats�ah!]�h#]��caveats�ah%]�h']�uh)h	hhhhhh*hK�ubeh}�(h]��kconfig-macro-language�ah!]�h#]��kconfig macro language�ah%]�h']�uh)h	hhhhhh*hKubah}�(h]�h!]�h#]�h%]�h']��source�h*uh)h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h*�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�N�character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  h�h�j�  j�  j/  j,  j.  j+  j�  j�  u�	nametypes�}�(j�  Nh�Nj�  Nj/  Nj.  Nj�  Nuh}�(j�  hh�h+j�  h�j,  j�  j+  j2  j�  j1  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]��transformer�N�
decoration�Nhhub.