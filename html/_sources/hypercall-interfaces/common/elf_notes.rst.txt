.. SPDX-License-Identifier: CC-BY-4.0

ELF notes
=========

.. doxygengroup:: elf_notes
   :project: Xen
   :members:
   :undoc-members:
