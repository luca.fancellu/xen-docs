.. SPDX-License-Identifier: CC-BY-4.0

Start-of-day shared data structure
==================================

.. doxygengroup:: start_of_day_shared_data_structure
   :project: Xen

.. doxygenstruct:: shared_info
   :project: Xen
   :members:
   :undoc-members:
